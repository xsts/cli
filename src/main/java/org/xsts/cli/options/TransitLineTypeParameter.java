/*
 * Group : XSTS
 * Project : Command Line Interface
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.cli.options;

public class TransitLineTypeParameter extends Parameter {
    public static final String  LONG_NAME = "transit-line-type";
    public static final String  SHORT_NAME = "tlt";
    public static final String  DESCRIPTION = "Set the  type of the transit line (experimental).";
    public static final String  VALUE_DESCRIPTION = "Bus, subway, etc.";
    public static final String  NAME = LONG_NAME;

    public TransitLineTypeParameter() {
        super(LONG_NAME, SHORT_NAME, DESCRIPTION, VALUE_DESCRIPTION);
    }
}

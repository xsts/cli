/*
 * Group : XSTS
 * Project : Command Line Interface
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.cli.options;

public class Parameter implements Option{
    public static final int TYPE = 2;
    public static final String EQUAL = "=";
    String name;
    String shortName;
    String value;
    String description;
    String valueDescription;

    boolean detected = false;

    public Parameter() {

    }

    public Parameter(String name, String shortName, String description, String valueDescription) {
        name(name);
        shortName(shortName);
        description(description);
        valueDescription(valueDescription);

    }

    public void name(String param) {
        this.name = param;
    }

    public void value(String param) {
        this.value = param;
    }

    public void shortName(String param) {
        this.shortName = param;
    }

    public String value() {
        return value;
    }

    @Override
    public int type() {
        return TYPE;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String shortName() {
        return  shortName;
    }


    @Override
    public int detect(String[] args) {
        for ( int i = 0; i < args.length; ++i) {
            String text = args[i];
            if (text.compareTo(dashedName())  == 0|| text.compareTo(dashedShortName()) == 0)
                return OPTION + i;
            if (text.contains(EQUAL)){
                String fragments[] = text.split(EQUAL);
                if ( fragments.length == 2) {
                    if ( fragments[0].compareTo(dashedName()) == 0 || fragments[0].compareTo(dashedShortName()) == 0)
                        return -i - OPTION;
                }
            }
        }
        return 0;
    }

    @Override
    public boolean extract(String[] args, int pos) {
        int i = pos;
        int realpos ;
        detected = false;
        if (i > 0) {
            realpos = i - OPTION;
            String text = args[i - OPTION];
            if (text.compareTo(dashedName()) == 0 || text.compareTo(dashedShortName()) == 0) {
                if (realpos < (args.length - 1)) {
                    value(args[realpos + 1]);
                    detected = true;
                }
            }
        } else if (i < 0) {
            realpos = -i - OPTION;
            String text = args[realpos];
            if (text.contains(EQUAL)){
                String fragments[] = text.split(EQUAL);
                if ( fragments.length == 2) {
                    if ( fragments[0].compareTo(dashedName()) == 0 || fragments[0].compareTo(dashedShortName()) == 0) {
                        value(fragments[1]);
                        detected = true;
                    }

                }
            }
        }

        return detected;
    }

    @Override
    public String dashedName() {
        return DASH +DASH + name;
    }

    @Override
    public String dashedShortName() {
        return DASH+shortName;
    }

    @Override
    public boolean detected() {
        return detected;
    }

    @Override
    public String description() {
        return description;
    }

    void description(String param) {
        description = param;
    }

    void valueDescription(String param) {
        valueDescription = param;
    }

    public String valueDescription() {
        return valueDescription;
    }
    public void reset() {
        detected = false;
        value = null;
    }
}

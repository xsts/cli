/*
 * Group : XSTS
 * Project : Command Line Interface
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.cli.options;

public class HelpFlag extends Flag {
    public static final String  LONG_NAME = "help";
    public static final String  SHORT_NAME = "h";
    public static final String  DESCRIPTION = "Show this help";
    public static final String  NAME = LONG_NAME;


    public HelpFlag() {
        super(LONG_NAME,SHORT_NAME, DESCRIPTION);
    }
}

/*
 * Group : XSTS
 * Project : Command Line Interface
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.cli.options;

public class Unknown implements Option{

    public static int TYPE = 0;

    @Override
    public int type() {
        return TYPE;
    }

    @Override
    public String name() {
        return "Unknown";
    }

    @Override
    public String shortName() {
        return null;
    }

    @Override
    public int detect(String[] args) {
        return 0;
    }

    @Override
    public boolean extract(String[] args, int pos) {
        return false;
    }

    @Override
    public String dashedName() {
        return null;
    }

    @Override
    public String dashedShortName() {
        return null;
    }

    @Override
    public boolean detected() {
        return false;
    }

    @Override
    public String description() {
        return null;
    }
}

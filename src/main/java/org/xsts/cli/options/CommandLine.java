/*
 * Group : XSTS
 * Project : Command Line Interface
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.cli.options;

import java.util.HashMap;
import java.util.List;
import java.util.Vector;

public class CommandLine {

    HashMap<String, Option> options = new HashMap<>();
    HashMap<String, Option> options2 = new HashMap<>();
    Vector<Option> optionsSorted = new Vector<>();
    Vector<String> localArguments = new Vector<>();

    public CommandLine() {

    }

    public void printUsage(String appName) {

        System.out.println("Usage:");
        System.out.println("");
        System.out.print(appName);

        System.out.println("\t\t[ option_1 ]...[ option_n ]");
        System.out.println("");
        System.out.print("List of options below:");
        for ( Option option: optionsSorted){
            System.out.println("");
            System.out.print("\t\t\t");
            System.out.print("[ < ");
            System.out.print(option.dashedName());

            if (!option.name().equals(option.shortName())){
                System.out.print(" | ");
                System.out.print(option.dashedShortName());
            }
            System.out.print(" >");


            if ( option.type() == Parameter.TYPE){
                Parameter parameter = (Parameter)option;
                System.out.print("\t");
                System.out.print("< ");
                System.out.print(parameter.valueDescription());
                System.out.print(" > ]");
            } else if (option.type() == Flag.TYPE) {
                System.out.print(" ]");
            }


            System.out.print("\t\t#");
            System.out.print(option.description());
        }
        System.out.println("");
    }

    public void add(Option option) {
        if (    (option != null) &&
                (option.type() == Flag.TYPE || option.type() == Parameter.TYPE) &&
                (options.containsKey(option.name()) == false)) {
            options.put(option.name(), option);
        }

        if (    (option != null) &&
                (option.type() == Flag.TYPE || option.type() == Parameter.TYPE) &&
                (options2.containsKey(option.shortName()) == false)) {
            options2.put(option.shortName(), option);
        }

        if (    ( option != null) &&
                (option.type() == Flag.TYPE || option.type() == Parameter.TYPE) ) {
            optionsSorted.add(option);
        }
    }

    public boolean optionDetected(String name) {
        if ( options.containsKey(name))
            return options.get(name).detected();
        else if ( options2.containsKey(name))
            return options2.get(name).detected();
        else
            return false;
    }

    public boolean booleanValue(String name) {
        BooleanType booleanType = null;
        Option option = null;
        if ( options.containsKey(name)) {
            option = options.get(name);
            if (option.type() == Parameter.TYPE){
                booleanType =  (BooleanType)option;
            }
        }
        else if ( options2.containsKey(name)) {
            option = options2.get(name);
            if (option.type() == Parameter.TYPE) {
                booleanType = (BooleanType) option;
            }
        }
        if (booleanType != null)
            return booleanType.booleanValue();
        return false;
    }

    public List<String> stringListValue(String name) {
        StringListType stringListType = null;
        Option option = null;
        if ( options.containsKey(name)) {
            option = options.get(name);
            if (option.type() == Parameter.TYPE){
                StringListFilterParameter filter = (StringListFilterParameter)option;
                stringListType =  (StringListType)filter;
            }
        }
        else if ( options2.containsKey(name)) {
            option = options2.get(name);
            if (option.type() == Parameter.TYPE) {
                StringListFilterParameter filter = (StringListFilterParameter)option;
                stringListType =  (StringListType)filter;            }
        }

        if (stringListType != null)
            return stringListType.listValue();
        return null;
    }

    public String optionValue(String name) {
        Parameter parameter = null;
        Option option = null;
        if ( options.containsKey(name)) {
            option = options.get(name);
            if (option.type() == Parameter.TYPE){
                parameter =  (Parameter)option;
            }
        }
        else if ( options2.containsKey(name)) {
            option = options2.get(name);
            if (option.type() == Parameter.TYPE) {
                parameter = (Parameter) option;
            }
        }
        if (parameter != null)
            return parameter.value();

        return null;
    }

    public Flag flag(String name){
        Option option = null;
        Flag flag = null;
        if ( options.containsKey(name))
            option = options.get(name);
        else if ( options2.containsKey(name))
            option = options2.get(name);

        if (option != null)
            flag = (Flag) option;
        return flag;
    }


    public Parameter parameter(String name){
        Option option = null;
        Parameter parameter = null;
        if ( options.containsKey(name))
            option = options.get(name);
        else if ( options2.containsKey(name))
            option = options2.get(name);

        if (option != null)
            parameter = (Parameter) option;
        return parameter;
    }

    public void resetLocalArguments() {
        localArguments = new Vector<>();
    }

    public boolean checkArguments(String[] args) {
        resetLocalArguments();
        copyToLocalArguments(args);

        for( Option opt : optionsSorted) {

            if (opt.type() == Flag.TYPE) {
                Flag flag = (Flag) opt;
                int pos =  flag.detect(args);
                if (pos > 0) {
                    localArguments.setElementAt("",pos - Option.OPTION);
                }

            } else if(opt.type() == Parameter.TYPE) {

                Parameter parameter = (Parameter) opt;
                int pos =  parameter.detect(args);
                if (pos > 0 ){
                    localArguments.setElementAt("",pos-Option.OPTION);
                    localArguments.setElementAt("",pos+1 -Option.OPTION);
                } else if (pos < 0) {
                    localArguments.setElementAt("",-pos-Option.OPTION);
                }
            }

        }

        for (String s : localArguments){
            if (s.equals("") == false) {
                return false;
            }
        }
        return true;
    }

    private void copyToLocalArguments(String[] args) {
        for ( String s : args) {
            String copyElement = new String(s);
            localArguments.add(copyElement);
        }
    }


    public void parseArguments(String[] args) {
        resetLocalArguments();
        copyToLocalArguments(args);

        for( String key : options.keySet()) {
            Option opt = options.get(key);
            if (opt.type() == Flag.TYPE) {
                Flag flag = (Flag) opt;
                int pos =  flag.detect(args);
                if (pos > 0) {
                    flag.extract(args, pos);
                }

            } else if(opt.type() == Parameter.TYPE) {
                Parameter parameter = (Parameter) opt;
                int pos =  parameter.detect(args);
                if (pos != 0) {
                    parameter.extract(args, pos);
                }
            }
        }

    }
}

/*
 * Group : XSTS
 * Project : Command Line Interface
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.cli.options;

public class Flag implements Option{
    public final static int TYPE = 1;
    String name = null;
    String shortName = null;
    String description;
    boolean detected = false;


    public Flag() {

    }

    public Flag(String name, String shortName, String description) {
        name(name);
        shortName(shortName);
        description(description);
    }

    public void name(String param) {
        this.name = param;
    }

    public void shortName(String param) {
        this.shortName = param;
    }

    @Override
    public int type() {
        return TYPE;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String shortName() {
        return shortName;
    }

    @Override
    public int detect(String[] args) {
        for ( int i = 0; i < args.length; ++i) {
            if (args[i].compareTo(dashedName()) == 0 || args[i].compareTo(dashedShortName()) == 0)
                return OPTION +i;
        }
        return 0;
    }

    @Override
    public boolean extract(String[] args, int pos) {
        detected = true;
        return detected;
    }

    @Override
    public String dashedName() {
        return DASH + DASH+name;
    }

    @Override
    public String dashedShortName() {
        return DASH+shortName;
    }

    @Override
    public boolean detected() {
        return detected;
    }

    @Override
    public String description() {
        return description;
    }


     void description(String param) {
        description = param;
    }

    public void reset() {
        detected = false;
    }
}

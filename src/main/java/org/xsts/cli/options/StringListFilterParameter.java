/*
 * Group : XSTS
 * Project : Command Line Interface
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.cli.options;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringListFilterParameter extends Parameter implements StringListType {
    public static final String ALL_VALUES = "all";
    public static final String VALUE_SEPARATOR = ",";

    ArrayList<String> typedValue = null;


    public StringListFilterParameter(String name, String shortName, String description, String valueDescription) {
        super(name, shortName, description, valueDescription);
    }


    @Override
    public boolean extract(String[] args, int pos) {
        boolean result = super.extract(args, pos);
        if (    value.compareTo(ALL_VALUES) == 0 ) {
            typedValue(null);
        }
        else {
            String[] arrayOfValues = value.split(VALUE_SEPARATOR);
            ArrayList<String> listOfValues = new ArrayList<String>(Arrays.asList(arrayOfValues));
            typedValue(listOfValues);
        }

        return result;
    }

    @Override
    public List<String> listValue() {
        return typedValue;
    }

    void typedValue(List<String> param) {
        typedValue = (ArrayList)param;
    }

    public List<String> typedValue() {
        return typedValue;
    }

}

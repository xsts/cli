/*
 * Group : XSTS
 * Project : Command Line Interface
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.cli.options;

public class YesNoParameter extends Parameter implements BooleanType{
    public static final String YES_OR_NO = "yes | y | no | n";
    public static final String LONG_YES = "yes";
    public static final String SHORT_YES = "y";
    public static final String LONG_NO = "no";
    public static final String SHORT_NO = "n";

    boolean typedValue = false;


    public YesNoParameter(String name, String shortName, String description) {
        super(name, shortName, description, YES_OR_NO);
    }

    @Override
    public boolean extract(String[] args, int pos) {
        boolean result = super.extract(args, pos);
        if (    value.compareTo(LONG_YES) == 0 ||
                value.compareTo(SHORT_YES) == 0) {
            typedValue(true);
        }
        else if (   value.compareTo(LONG_NO) == 0 ||
                    value.compareTo(SHORT_NO) == 0  ) {
            typedValue(false);
        }
        else {
            value = "Undefined";
            result = false;
        }
        return result;
    }

    void typedValue(boolean param) {
        typedValue = param;
    }

    public boolean typedValue() {
        return typedValue;
    }

    @Override
    public boolean booleanValue() {
        return typedValue;
    }
}

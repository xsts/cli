/*
 * Group : XSTS
 * Project : Command Line Interface
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.cli.options;

public interface Option {
    static final String DASH = "-";
    static final int OPTION = 1000000;
    int type();
    String name();
    String shortName();
    int detect(String [] args);
    boolean extract(String [] args, int pos);
    String dashedName();
    String dashedShortName();
    boolean detected();
    String description();
}

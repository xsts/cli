/*
 * Group : XSTS
 * Project : Command Line Interface
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.cli.about;


import org.xsts.core.about.InfoTransPub;
import org.xsts.core.terminal.TextLine;
import org.xsts.core.terminal.TextWindow;

public class AboutCLI {
    public static final String PROJECT_NAME = "Command Line Interface (CLI)";
    public static final String VERSION = "0.2.11";

    public static void main(String ... args) {
        TextWindow tw = new TextWindow(80, 3);
        tw.addTextLine(new TextLine(projectName()));
        tw.addTextLine(new TextLine(currentVersion()));
        tw.addTextLine(new TextLine(memberOf()));
        tw.addTextLine(new TextLine(organizationName()));
        tw.addTextLine(new TextLine(sinceYear()));
        tw.print();
    }

    static  String organizationName(){
        return new StringBuilder()
                .append("Author: ")
                .append(InfoTransPub.FULL_ORGANIZATION_NAME)
                .toString();
    }

    static  String memberOf() {
        return new StringBuilder()
                .append("Framework: ")
                .append(InfoTransPub.SHORT_GROUP_NAME)
                .toString();
    }

    static  String projectName() {
        return new StringBuilder()
                .append("Module: ")
                .append(PROJECT_NAME)
                .toString();
    }

    static  String sinceYear() {
        return new StringBuilder()
                .append("Since: ")
                .append("2016")
                .toString();
    }

    static  String currentVersion() {
        return new StringBuilder()
                .append("Version: ")
                .append(VERSION)
                .toString();
    }
}

/*

How to use CLI for command line options
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Note: see project STV for a practical use case of the command line interface
Obs: All the XSTS apps (not libraries)  can be used:
 - either from the command line;
 - or as a library.

import org.xsts.cli.options.CommandLine;
import org.xsts.cli.options.Flag;
import org.xsts.cli.options.Option;
import org.xsts.cli.options.Parameter;

import java.util.HashMap;

. . . . . . .

String arguments[] = {"-help", "-i=...path"};

HashMap<String, Option> options = new HashMap<>();

Option optHelp = new Flag("help","h", "Show this help");
Option optInput = new Parameter("input","i", "Set input file", "InputFilePath");

options.put(optHelp.name(), optHelp);
options.put(optInput.name(), optInput);

CommandLine cmdLine = new CommandLine();
cmdLine.add(optHelp);
cmdLine.add(optInput);

cmdLine.printUsage("CLI interface");

if (cmdLine.checkArguments(arguments) == false) {
    System.out.println("There is a bad argument here");
} else {
    cmdLine.parseArguments(arguments);
    if (cmdLine.optionDetected("h")){
        System.out.println("Option : " + cmdLine.flag("h").name());
    }

    if (cmdLine.optionDetected("i")){
        System.out.println("Option : " + cmdLine.parameter("i").name() + " ==> " + cmdLine.parameter("i").value());
    }
}

 */
